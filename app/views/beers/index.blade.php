@extends('layouts.app')

@section('content')
    <h1>Beer List</h1>
    <a href="/beers/create" class=btn btn-primary>Add New</a>

    @if(count($beers)>0) 
        <table style="width:75%;" border="1">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>ABV</th>
                    <th>IBU</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                @foreach($beers as $beer)
                    <tr>
                        <td><a href="/beers/{{$beer->id}}">{{$beer->name}}</a></td>
                        <td>{{$beer->abv}}</td>
                        <td>{{$beer->ibu}}</td>
                        <td>{{$beer->description}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>No beers found</p>
    @endif
@endsection
