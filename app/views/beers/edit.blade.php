@extends('layouts.app')

@section('content')
    <h1>Edit Beer</h1>
    {{ Form::open(array('action' => array('BeersController@update',$beer->id),'method'=>'PUT')) }}

    
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', Input::old('name')??$beer->name, array('class' => 'form-control')) }}
<br>

        {{ Form::label('abv', 'ABV') }}
        {{ Form::text('abv', Input::old('abv')??$beer->abv, array('class' => 'form-control')) }}

        <br>

        {{ Form::label('ibu', 'IBU') }}
        {{ Form::text('ibu', Input::old('ibu')??$beer->ibu, array('class' => 'form-control')) }}
<br>
        {{ Form::label('description', 'Description') }}
        {{ Form::text('description', Input::old('description')??$beer->description, array('class' => 'form-control')) }}
<br>
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    <br>
    <br>
    <a href="../"><- Back</a>
@endsection
