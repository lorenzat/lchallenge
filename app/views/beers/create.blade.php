@extends('layouts.app')

@section('content')
    <h1>Create Beer</h1>
    {{ Form::open(array('action' => 'BeersController@store','method'=>'POST')) }}

    
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
<br>

        {{ Form::label('abv', 'ABV') }}
        {{ Form::text('abv', Input::old('abv'), array('class' => 'form-control')) }}

        <br>

        {{ Form::label('ibu', 'IBU') }}
        {{ Form::text('ibu', Input::old('ibu'), array('class' => 'form-control')) }}
<br>
        {{ Form::label('description', 'Description') }}
        {{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
<br>
        {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    <br>
    <br>
    <a href="../"><- Back</a>
@endsection
