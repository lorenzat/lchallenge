@extends('layouts.app')

@section('content')
    <h1>{{$beer->name}}</h1>

    {{ Form::open(array('action' => array('BeersController@edit',$beer->id),'method'=>'GET')) }}
        {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

    {{ Form::open(array('action' => array('BeersController@destroy',$beer->id),'method'=>'DELETE')) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

    <p>ABV: {{$beer->abv}}%</p>
    <p>IBU: {{$beer->ibu}}</p>
    <p>Description: {{$beer->description}}</p>

    <br>
    <br>
    
    <a href="../"><- Back</a>
@endsection
