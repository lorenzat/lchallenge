<?php


class BeersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$beers = Beer::orderBy('name','asc')->get();
		return View::make('beers.index')->with('beers',$beers);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('beers.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name'       => 'required',
            'abv'      => 'numeric',
            'ibu' => 'numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('beers/create')
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {
            // store
            $beer = new Beer;
            $beer->name       = Input::get('name');
            $beer->abv      = Input::get('abv');
			$beer->ibu = Input::get('ibu');
			$beer->description = Input::get('description');
            $beer->save();

            // redirect
            Session::flash('message', 'Successfully created beer!');
            return Redirect::to('beers');
		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$beer=Beer::find($id);
		return View::make('beers.show')->with('beer',$beer);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$beer=Beer::find($id);
		return View::make('beers.edit')->with('beer',$beer);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$rules = array(
            'name'       => 'required',
            'abv'      => 'numeric',
            'ibu' => 'numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('beers/create')
                ->withErrors($validator)
                ->withInput(Input::all());
        } else {
            // store
			$beer=Beer::find($id);

            $beer->name       = Input::get('name');
            $beer->abv      = Input::get('abv');
			$beer->ibu = Input::get('ibu');
			$beer->description = Input::get('description');
            $beer->save();

            // redirect
            Session::flash('message', 'Successfully saved beer!');
            return Redirect::to('beers');
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Beer::destroy($id);
		return Redirect::to('/beers');
	}


}
